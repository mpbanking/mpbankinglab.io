let numFormat = Intl.NumberFormat("de", {})

function formatNum(num){
    return numFormat.format(num) + " ¤"
}


function clickNumberButton(button){
    let numberField = button.parentElement.querySelector(".number-field");

    let buttonValue = parseInt(button.dataset.num)

    let normalized = numberField.value.replace(/\D/g, "")

    if (normalized.length >= 15)
        return

    let number = parseInt(normalized)
    if (isNaN(number)){
        number = 0;
    }

    let nextValue = number * 10 + buttonValue;

    numberField.value = formatNum(nextValue)
}

function clickBackspace(button){
    let numberField = button.parentElement.querySelector(".number-field");

    let number = parseInt(numberField.value.replace(/[.,]/g, ""))
    if (isNaN(number)){
        number = 0;
    }

    let nextValue = Math.floor(number / 10);

    numberField.value = formatNum(nextValue)
}

function clickAccept(button){
    let numberInput = button.parentElement
    let numberField = numberInput.querySelector(".number-field");

    numberInput.dispatchEvent(new CustomEvent("num-okay", { detail: parseInt(numberField.value.replace(/[.,]/g, "")), bubbles: true }))
}

function keepFocus(element){
    if (element.parentElement != null){
        window.setTimeout(() => element.focus());
    }
}

function nfcInput(element){
    let text = element.value;
    if (text.endsWith("\n")){
        let trimmed = text.trim()
        if (trimmed.length == 0)
            return;
        element.parentElement.dispatchEvent(new CustomEvent("nfc-okay", { detail: trimmed, bubbles: true }));
        element.value = "";
    }
}

function fixNfcFocus(){
    let nfc = document.querySelector(".nfc-field");
    if (nfc != null && nfc != document.activeElement){
        nfc.focus()
    }
    window.setTimeout(fixNfcFocus, 64)
}

fixNfcFocus()

const gameState = $fuwuProxy()
gameState.players = []
gameState.page = "home"
gameState.started = false
gameState.balanceText = ""
gameState.transferAmount = 0
gameState.transferPlayer = {}
gameState.recipientPlayer = {}
gameState.validationError = ""

const startPage = $fuwuProxy()
startPage.page = "start"
startPage.addName = ""
startPage.validationError = ""

let loadedGame = window.localStorage.getItem("gameState")
if (loadedGame != null){
    loadedGame = JSON.parse(loadedGame)
    for (const key in loadedGame) {
        if (!key.startsWith("$")){
            gameState[key] = loadedGame[key]
        }
    }
}

gameState.$listeners.push(() => {
    window.localStorage.setItem("gameState", JSON.stringify(gameState.$target))
})

pageSwitch("home")

class Player {
    constructor(name, nfc) {
        this.name = name;
        this.nfcTag = nfc;
        this.balance = 0;
    }
}

function pageSwitch(page){
    gameState.page = page
    gameState.balanceText = ""
    gameState.validationError = ""
}

function startSwitch(page){
    startPage.page = page
    startPage.validationError = ""
}

function resetGame(){
    gameState.started = false;
    gameState.players = [];
    pageSwitch("home");
    startSwitch("start")
}

function findPlayer(nfcTag){
    return gameState.players.find(p => p.nfcTag == nfcTag);
}

function homeNfc(nfcTag){
    const player = findPlayer(nfcTag)
    if (player != null){
        gameState.validationError = ""
        gameState.balanceText = `${player.name}: ${formatNum(player.balance)}`
    } else {
        gameState.validationError = "Unbekannte Spielerkarte"
    }
}

function bankInAmount(amount){
    if (amount > 0){
        gameState.transferAmount = amount;
        pageSwitch("bank-in-2");
    } else {
        gameState.validationError = "Betrag zu gering"
    }
}

function bankInPlayer(nfcTag){

    const player = findPlayer(nfcTag);

    if (player != null){
        if (player.balance >= gameState.transferAmount){
            gameState.transferPlayer = player;
            player.balance -= gameState.transferAmount;
            pageSwitch("transfer-success-single");
        } else {
            gameState.validationError = `Kontostand von ${player.name} zu gering: ${formatNum(player.balance)}`
        }
    } else {
        gameState.validationError = "Unbekannte Spielerkarte"
    }
}

function bankOutAmount(amount){
    if (amount > 0){
        gameState.transferAmount = amount;
        pageSwitch("bank-out-2");
    } else {
        gameState.validationError = "Betrag zu gering"
    }
}

function bankOutPlayer(nfcTag){

    const player = findPlayer(nfcTag);

    if (player != null){
        gameState.transferPlayer = player;
        player.balance += gameState.transferAmount;
        pageSwitch("transfer-success-single");
    } else {
        gameState.validationError = "Unbekannte Spielerkarte"
    }
}

function transferSender(nfcTag){
    const player = findPlayer(nfcTag);

    if (player != null){
        gameState.transferPlayer = player;
        pageSwitch("transfer-1");
    } else {
        gameState.validationError = "Unbekannte Spielerkarte"
    }
}

function transferAmount(amount){
    if (amount > 0){
        if (gameState.transferPlayer.balance >= amount){
            gameState.transferAmount = amount;
            pageSwitch("transfer-2");
        } else {
            gameState.validationError = "Kontostand zu gering"
        }
        
    } else {
        gameState.validationError = "Betrag zu gering"
    }
}

function transferAccept(nfcTag){
    const player = findPlayer(nfcTag);

    if (player != null){
        if (player.nfcTag != gameState.transferPlayer.nfcTag){
            gameState.recipientPlayer = player;
            player.balance += gameState.transferAmount;
            gameState.transferPlayer.balance -= gameState.transferAmount;
            pageSwitch("transfer-success-duo");
        } else {
            gameState.validationError = "Anderen Empfänger wählen"
        }
    } else {
        gameState.validationError = "Unbekannte Spielerkarte"
    }
}

function startName(){
    let name = document.querySelector(".name-enter").value
    if (name.length > 1){
        startPage.addName = name
        startSwitch("add-nfc")
    } else {
        startPage.validationError = "Name zu kurz"
    }
}

function startNfc(nfcTag){
    if (gameState.players.filter(p => p.nfcTag == nfcTag).length == 0){
        const player = new Player(startPage.addName, nfcTag);
        gameState.players.push(player);
        startSwitch("start");
    } else {
        startPage.validationError = "Neue Karte auflegen"
    }
}

function popPlayer(player){
    gameState.players = gameState.players.filter(pl => player.nfcTag != pl.nfcTag)
}

function startGame(balance){
    if (!isNaN(balance)){
        gameState.players.forEach(pl => {
            pl.balance = balance
        });
        gameState.started = true;
    } else {
        startPage.validationError = "Startguthaben eingeben"
    }
}

function toggleFullscreen(){
    if (!!document.fullscreenElement){
        document.exitFullscreen()
    } else {
        document.body.requestFullscreen()
    }
}

function onResize(){
    if (window.innerWidth >= window.innerHeight && window.innerHeight < 600){
        document.body.setAttribute("dsp-mode", "horizontal")
    } else {
        document.body.setAttribute("dsp-mode", "vertical")
    }
}

window.addEventListener("resize", onResize);
window.addEventListener("load", onResize);